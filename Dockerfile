FROM debian

ARG INITIAL_DELAY=2
ARG SUBSEQUENT_DELAY=.3

RUN apt update && apt install curl procps -y
RUN curl -L https://itunesconnect.apple.com/WebObjects/iTunesConnect.woa/ra/resources/download/public/Transporter__Linux/bin -o transporter.sh

COPY install-transporter.sh .

RUN ./install-transporter.sh -i $INITIAL_DELAY -s $SUBSEQUENT_DELAY
RUN rm transporter.sh

ENV PATH="${PATH}:/usr/local/itms/bin/"
