#!/bin/bash
# version="0.1"
#
# This is an optional arguments-only example of Argbash potential
#
# ARG_OPTIONAL_SINGLE([initial-delay],[i],[Initial prompt delay],[2])
# ARG_OPTIONAL_SINGLE([subsequent-delay],[s],[Subsequent prompt delay],[.3])
# ARG_VERSION([echo test v$version])
# ARG_HELP([The general script's help msg])
# ARGBASH_GO()
# needed because of Argbash --> m4_ignore([
### START OF CODE GENERATED BY Argbash v2.9.0 one line above ###
# Argbash is a bash code generator used to get arguments parsing right.
# Argbash is FREE SOFTWARE, see https://argbash.io for more info
# Generated online by https://argbash.io/generate


die()
{
	local _ret="${2:-1}"
	test "${_PRINT_HELP:-no}" = yes && print_help >&2
	echo "$1" >&2
	exit "${_ret}"
}


begins_with_short_option()
{
	local first_option all_short_options='isvh'
	first_option="${1:0:1}"
	test "$all_short_options" = "${all_short_options/$first_option/}" && return 1 || return 0
}

# THE DEFAULTS INITIALIZATION - OPTIONALS
_arg_initial_delay="2"
_arg_subsequent_delay=".3"


print_help()
{
	printf '%s\n' "The general script's help msg"
	printf 'Usage: %s [-i|--initial-delay <arg>] [-s|--subsequent-delay <arg>] [-v|--version] [-h|--help]\n' "$0"
	printf '\t%s\n' "-i, --initial-delay: Initial prompt delay (default: '2')"
	printf '\t%s\n' "-s, --subsequent-delay: Subsequent prompt delay (default: '.3')"
	printf '\t%s\n' "-v, --version: Prints version"
	printf '\t%s\n' "-h, --help: Prints help"
}


parse_commandline()
{
	while test $# -gt 0
	do
		_key="$1"
		case "$_key" in
			-i|--initial-delay)
				test $# -lt 2 && die "Missing value for the optional argument '$_key'." 1
				_arg_initial_delay="$2"
				shift
				;;
			--initial-delay=*)
				_arg_initial_delay="${_key##--initial-delay=}"
				;;
			-i*)
				_arg_initial_delay="${_key##-i}"
				;;
			-s|--subsequent-delay)
				test $# -lt 2 && die "Missing value for the optional argument '$_key'." 1
				_arg_subsequent_delay="$2"
				shift
				;;
			--subsequent-delay=*)
				_arg_subsequent_delay="${_key##--subsequent-delay=}"
				;;
			-s*)
				_arg_subsequent_delay="${_key##-s}"
				;;
			-v|--version)
				echo test v$version
				exit 0
				;;
			-v*)
				echo test v$version
				exit 0
				;;
			-h|--help)
				print_help
				exit 0
				;;
			-h*)
				print_help
				exit 0
				;;
			*)
				_PRINT_HELP=yes die "FATAL ERROR: Got an unexpected argument '$1'" 1
				;;
		esac
		shift
	done
}

parse_commandline "$@"

{
  sleep $_arg_initial_delay
  for i in {1..20}
  do
    printf '\033[B'
    sleep $_arg_subsequent_delay
  done
  for i in {1..3}
  do
    printf 'yes\n'
    sleep $_arg_subsequent_delay
  done
} | script -c 'sh /transporter.sh'
