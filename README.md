# Welcome

## What is Transporter?

Transporter is Apple’s Java-based command-line tool for large catalog deliveries. You can use Transporter to deliver your pre-generated content in a Store Package to the Apple TV app, iTunes Store, Apple Books, and App Store.

Whether you use your iTunes Connect account (book publisher, music provider), App Store Connect account (app developer), or encoding house account for your book, video, music, or app content, you can use Transporter to ensure your metadata and assets (for example, audio, video, books, and app files) are properly delivered to the iTunes Store, Apple Books, or App Store, validating the Store Packages against Apple’s specifications. Because some Transporter options are restricted based on user type and content type, before you deliver your Store Packages, make sure you know whether you want Transporter to authenticate you using your iTunes Connect or App Store Connect account, or your encoding house account. For more information, see Overview.

Transporter includes the following features:

- An easy-to-use, out-of-the-box installation package, including installers for macOS, Microsoft’s Windows, and Red Hat Enterprise Linux.
- Asset tracking. Transporter validates the list of assets (for example, cover art, audio files, video files, In-App Purchases, Game Center) you reference in your metadata file against the files you actually deliver to the iTunes Store, Apple Books, and App Store. Using the web service, Transporter validates the list of files against the received metadata and returns an error message if the list doesn’t match the information in the metadata. For example, `“Delivered file set does not contain expected file [34D17614CB805E8AB853CD5ABCBF3C0A-940AE112-01-18-mp4-128000.m4a] that is referenced in the metadata file”`.
- Automatic software update. Transporter contains its own an automatic software update. When you launch and run Transporter, software update automatically checks the Transporter version, updates your installation if needed, and displays logging information in the console.
- Checksum validation of assets. You should include checksums for each file you deliver in the metadata file you send to the iTunes Store, Apple Books, and App Store. Transporter uses this checksum information to validate each file you send against the expected dynamically generated checksums from the Transporter.
- Error summary output. Transporter displays error summary information for Store Packages that weren’t successfully uploaded. In addition to a list of unsuccessful package names, Transporter displays summary text containing error messages at the end of an upload attempt. For example:
```
Package Summary:

2 package(s) were not uploaded because they had problems:
/Users/bsmith/Applications/Packages/Many/00000000001041.itmsp - Error Messages:
Unable to find registered encoding house user for username "bsmith" (1001)
/Users/bsmith/Applications/Packages/Many/00000000001042.itmsp - Error Messages:
Unable to find registered encoding house user for username "bsmith" (1001)
``` 
- Progress indicator details. Transporter includes a progress indicator option so you can specify whether or not you want Transporter to display progress details during upload.
- Resumable uploads. If a network connection fails or a server goes offline during an upload, Transporter can resume the upload at a later time.

More information about Apple Transporter can be found [here](https://help.apple.com/itc/transporteruserguide/en.lproj/static.html).
